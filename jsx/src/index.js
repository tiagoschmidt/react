import React from 'react';
import ReactDOM from 'react-dom';

const App = () => {
    const vars = {
            btnText: 'Click', 
            lblText: 'Enter Name:', 
            style: { backgroundColor: 'blue', color: 'white' }
        };

    return (
        <div>
            <label className="label" htmlFor="input">
                {vars.lblText}
            </label>
            <input id="input" type="text"/>
            <button style={vars.style}>
                {vars.btnText}
            </button>
        </div>
    );
};

ReactDOM.render(
    <App />,
    document.querySelector('#root')
);