import React from 'react'
import UnsplashAPI from '../api/unsplashAPI'
import SearchBar from './SearchBar'
import ImageList from './ImageList'

class App extends React.Component {
  state = { images: [] }

  onSearchSubmit = async term => {
    const imagesFound = await new UnsplashAPI().searchImages(term) 
    this.setState({ images: imagesFound })
  }

  render(){
    return (
      <div className="ui container">
        <SearchBar onSubmit={this.onSearchSubmit}/>
        <ImageList images={this.state.images}/>
      </div>
    )
  }
}

export default App