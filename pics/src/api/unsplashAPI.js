import axios from 'axios'

class UnsplashAPI {
  baseURL = 'https://api.unsplash.com/'
  //Client-ID + My Access key from UnsplashAPI account
  authorization = 'Client-ID 4ed7e1597ae2b55753ab14acd0ff8202bb84f81cab758f810c4b1a70a79cfe05'

  searchImages = async (term) => {
    const response = await axios.get(this.baseURL + 'search/photos', {
      params: { query: term },
      headers: {
        Authorization: this.authorization
      }
    })
    return response.data.results
  }

}

export default UnsplashAPI 