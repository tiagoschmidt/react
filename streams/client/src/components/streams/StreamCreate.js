import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { createStream } from '../../actions'
import StreamForm from './StreamForm'

class StreamCreate extends React.Component {
  onSubmit = formValues => {
    this.props.createStream(formValues)
  }

  render(){
    return !this.props.isSignedIn ? 
      <Redirect to="/" /> :
    (
      <StreamForm onSubmit={this.onSubmit}/>
    )
  }
}

const mapStateToProps = state => {
  return { isSignedIn: state.auth.isSignedIn }
}

export default connect(mapStateToProps, { createStream })(StreamCreate)