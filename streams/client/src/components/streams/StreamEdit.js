import React from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { fetchStream, updateStream } from '../../actions'
import StreamForm from './StreamForm'

class StreamEdit extends React.Component {
  componentDidMount(){
    this.props.fetchStream(this.props.match.params.id)
  }

  onSubmit = formValues => {
    this.props.updateStream(formValues, this.props.match.params.id)
  }

  render(){        
    return !this.props.isSignedIn ?
      <Redirect to="/" /> :
    (
      <div>
        <StreamForm onSubmit={this.onSubmit} initialValues={this.props.stream}/>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return { 
    isSignedIn: state.auth.isSignedIn,
    stream: state.streams[ownProps.match.params.id]
  }
}

export default connect(mapStateToProps, { fetchStream, updateStream})(StreamEdit)