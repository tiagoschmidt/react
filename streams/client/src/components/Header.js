import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import GoogleAuth from './GoogleAuth'
 
class Header extends React.Component {
  renderCreateOption(){
    if(this.props.isSignedIn)
      return (
        <Link to="/streams/new" className="item">
          Create a Stream
        </Link>
      )
  }

  render(){
    return (
      <div className="ui secondary pointing menu">
        <Link to="/" className="item">
          Home
        </Link>
        <div className="right menu"></div>
        {this.renderCreateOption()}      
        <GoogleAuth />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { isSignedIn: state.auth.isSignedIn }
}

export default connect(mapStateToProps)(Header)