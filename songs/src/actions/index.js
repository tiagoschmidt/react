//Action creator, returns an Action (JS object))
export const selectSong = song => {
  return {
    type: 'SONG_SELECTED',
    payload: song
  }
}

