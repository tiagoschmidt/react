import React from 'react';
import ReactDOM from 'react-dom';
import CommentDetail from './CommentDetail';
import faker from 'faker';
import ApprovalCard from './ApprovalCard';

const App = () => {
    return (
        <div className="ui container comments">
            <ApprovalCard>
                <CommentDetail avatar={faker.image.avatar()} author="Sam" timeAgo="Today at 12:00PM" content="Nice post!"/>
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail avatar={faker.image.avatar()} author="Alex" timeAgo="Today at 07:00PM" content="That's my commentary"/>
            </ApprovalCard>
            <ApprovalCard>
                <CommentDetail avatar={faker.image.avatar()} author="Jane" timeAgo="Yesterday at 02:00AM" content="A regular post on this blog."/>
            </ApprovalCard>
        </div>
    )
};

ReactDOM.render(
    <App/>,
    document.querySelector('#root')
);