import axios from '../apis/unsplashAPI'
import { FETCH_IMAGES, SIGN_IN, SIGN_OUT } from './types'

export const searchImages = term => async dispatch => {
  const response = await axios.get('search/photos', { params: { query: term } })

  dispatch({ type: FETCH_IMAGES, payload: response.data.results })
}

export const signIn = userId => {
  return {
    type: SIGN_IN,
    payload: userId
  }
}

export const signOut = () => {
  return {
    type: SIGN_OUT
  }
}