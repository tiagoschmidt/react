import axios from 'axios'

export default axios.create({
  baseURL: 'https://api.unsplash.com/',
  headers: {
    Authorization: 'Client-ID 289467307643-go1jgf83rf7nd9namnnb08q04dvk5hjp.apps.googleusercontent.com'
  }
})