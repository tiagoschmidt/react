import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import '../../css/guestScreen.css'
import GoogleAuth from '../GoogleAuth'
import Card from '../Card'

class GuestScreen extends React.Component {
  render() {    
    return this.props.isSignedIn ?
      <Redirect to="/home" /> : 
    (
      <div className="home">
        <Card 
          headerTxt='Sign In First'
          metaTxt='You must be logged in'
          descriptionTxt='Sign in with your google account to start using this amazing application'
        >
        <div className="google-btn">
          <GoogleAuth />
        </div>
        </Card>
      </div>  
    )
  }
}

const mapStateToProps = state => {
  return { isSignedIn: state.auth.isSignedIn }
}

export default connect(mapStateToProps)(GuestScreen)