import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { searchImages } from '../../actions'
import Header from '../Header'
import SearchBar from '../SearchBar'
//import ImageList from './ImageList' 

class PicsScreen extends React.Component {
  onSearchSubmit = term => {
    this.props.searchImages(term)
  }

  render(){
    return !this.props.isSignedIn ? 
      <Redirect to="/" /> : 
    (
      <div className="ui container">
        <Header />
        <SearchBar onSubmit={this.onSearchSubmit}/>
        {/*<ImageList images={this.props.images}/> */}
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { images: state.images, isSignedIn: state.auth.isSignedIn }
}

export default connect(mapStateToProps, {searchImages})(PicsScreen)