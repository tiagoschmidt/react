import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import Header from '../Header'

class VideosScreen extends React.Component {
  render(){
    return !this.props.isSignedIn ? 
      <Redirect to="/" /> : 
    (
      <div className="ui container">
        <Header />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { isSignedIn: state.auth.isSignedIn }
}

export default connect(mapStateToProps)(VideosScreen)