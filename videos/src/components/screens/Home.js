import React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import Card from '../Card'
import '../../css/card.css'
import '../../css/home.css'
import Header from '../Header';
import Button from '../Button';

class Home extends React.Component {
  render(){
    return !this.props.isSignedIn ?
      <Redirect to="/" /> : 
    (
      <div>
        <Header />
        <div className="home">
          <div className="cards-display">
            <div className="home-card">
              <Card 
                headerTxt='Watch Videos'
                metaTxt='Only the best'
                descriptionTxt='Watch the best videos direct from "Youtube Api"!'
              >
                <div className="btn-home">
                  <Button linkPath="/videos" />
                </div>
              </Card>
            </div>
            <div className="home-card">  
              <Card 
                headerTxt='Search for Images'
                metaTxt='The images you want'
                descriptionTxt='Search for all images you possibly want! All with the "Unsplash Api"'
              >
                <div className="btn-home">
                  <Button linkPath="/pictures" className="btn-home"/>
                </div>
              </Card>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { isSignedIn: state.auth.isSignedIn }
}

export default connect(mapStateToProps)(Home)