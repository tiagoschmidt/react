import React from 'react'
import '../css/card.css'

const Card = props => {
    
  return (    
    <div className="ui card">
      <div className="ui content">
        <div className="ui header">
          {props.headerTxt}
        </div>
        <div className="ui meta">
          {props.metaTxt}
        </div>
        <div className="ui description">
          {props.descriptionTxt}
        </div>
        <div>
          {props.children}
        </div>
      </div>
    </div>
  )
}

export default Card