import React from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'

class SearchBar extends React.Component {
  onFormSubmit = event => {
    event.preventDefault()
    this.props.onSubmit(this.props.searchTerm)
  }

  render(){
    return (
      <div className="ui segment">
        <form onSubmit={this.onFormSubmit} className="ui form">
          <div className="field">
            <label>Make Your Search</label>
            <input type="text" value={this.state.term} onChange={(e) => this.setState({ term: e.target.value })}/>
          </div>
        </form>
      </div>
    )
  }

}

const mapStateToProps = state => {
  return { searchTerm: state.searchTerm }
}

export default connect(mapStateToProps)(SearchBar)