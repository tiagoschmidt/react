import React from 'react'
import { Link } from 'react-router-dom'
import GoogleAuth from './GoogleAuth'
import '../css/header.css'
 
const Header = () => {
  return (
    <div className="ui secondary pointing menu">
      <Link to="/home" className="item">
        Home
      </Link>
      <Link to="/videos" className="item">
        Videos
      </Link>
      <Link to="/pictures" className="item">
        Images
      </Link>
      <div className="right menu"></div>
      <div className="mt">
        <GoogleAuth />
      </div>
    </div>
  )
}

export default Header