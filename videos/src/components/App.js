import React from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import VideosScreen from './screens/VideosScreen'
import PicsScreen from './screens/PicsScreen'
import GuestScreen from './screens/GuestScreen'
import Home from './screens/Home'

const App = () => {
  return (
    <BrowserRouter>
      <div className="ui container">
        <Route path="/" exact component={GuestScreen}/>
        <Route path="/home" exact component={Home}/>
        <Route path="/videos" exact component={VideosScreen}/>
        <Route path="/pictures" exact component={PicsScreen}/>
      </div>
    </BrowserRouter>
  )
}

export default App