import React from 'react'
import { Link } from 'react-router-dom'

const Button = props => {
  return (
    <Link to={props.linkPath}>
      <button className="ui secondary green button">
        Go now!
      </button>
    </Link>
  )
}

export default Button