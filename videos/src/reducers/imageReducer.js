import { FETCH_IMAGES } from '../actions/types'

const INITIAL_STATE = {
  images: []
}

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
      case FETCH_IMAGES:
        return { ...state, images: action.payload }
      default:
        return state
    }
}